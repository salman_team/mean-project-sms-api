var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var path = require('path');
var cookieParser = require('cookie-parser');
var http = require('http');
var port = process.argv[2] || 8080;
var expressSession = require('express-session');
var mongoose = require('mongoose');
var mongooseConfig = require('./server/configs/mongoose.config')


var app = express();
mongoose.connect(mongooseConfig.url)



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'))
app.use(express.static(path.join(__dirname, 'dist')));

require('./server/demo')(app)
require('./server/index')(app)
app.use('/webhooks', function(req, res, next){
    console.log(req.body);
    res.status(200).end('success')
})
app.use('/*', express.static(path.join(__dirname, 'dist')));


app.use(function (req, res, next) {
    var err = new Error('File Not Found ');
    err.status = 404;
    next(err);
})

app.use(function (err, req, res, next) {
    console.log(err);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') == 'development' ? err : '';
    res.status(err.status || 500);
    res.send({ result: 'error', error: err, status: 500 });

})


const server = http.createServer(app);
server.listen(port);
server.on('listening', function () {
    console.log(`server listen successfully ${port}`)
})

const domain = require('domain').create();

domain.run(() => {
    setTimeout(function () {
        console.log(`error throwing `)
        // throw new Error('my error ');
    }, 3000)
})

domain.on('error', function (err) {
    console.log(`error catch block`)
    console.log(err);
})


module.exports = app;