import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';
import { MessagesComponent } from './messages.component';
import { GroupsComponent } from '../../components/groups/groups.component';
import { ContactsComponent } from '../../components/contacts/contacts.component';
import { SendmessageComponent } from '../../components/sendmessage/sendmessage.component';
import { CampaginsComponent } from '../../components/campagins/campagins.component';

const router: Routes = [
    {
        path: '', component: MessagesComponent,
        children: [
            { path: '', redirectTo: 'groups' },
            { path: 'groups', component: GroupsComponent, },
            { path: 'groups/contacts/:id', component: ContactsComponent, },
            { path: 'sendmessage', component: SendmessageComponent },
            { path: 'campagins', component: CampaginsComponent }
        ]
    }
]


@NgModule({
    declarations: [
        MessagesComponent,
        GroupsComponent,
        ContactsComponent,
        CampaginsComponent,
        SendmessageComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(router),
        SharedModule,
        FormsModule,
        ReactiveFormsModule
    ]
})

export class MessagesModule {

}