import { NgModule } from '@angular/core';
import { MessagesService } from './messages.service';
import { GroupsdataService } from './groupsdata.service';
import { ContactsdataService } from './contactsdata.service';
// import { SendmessageService } from './sendmessage.service';

@NgModule({
    providers: [
        MessagesService,
        GroupsdataService,
        ContactsdataService,
        
    ]

})

export class SharedModule {

}