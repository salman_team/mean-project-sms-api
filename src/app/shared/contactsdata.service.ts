import { Injectable, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { saveAs } from 'file-saver';


@Injectable()

export class ContactsdataService implements OnInit {
    public contactObservable = new Subject();
    public contactList;
    private url = '/api/contacts';
    private getObserver;
    private insertObserver;
    private deletetObserver;
    private updateObserver;
    private downloadObserver;
    constructor(private MessagesService: MessagesService) {

    }

    ngOnInit() {

    }

    getContacts(id) {
        this.getObserver = this.MessagesService.getBackground(this.url + '/' + id)
            .subscribe(data => {
                this.contactList = data;
                this.contactObservable.next(this.contactList);
            })

    }
    // id is group id 
    insert(id, values) {
        this.insertObserver = this.MessagesService.postBackground(this.url, values)
            .subscribe(data => {
                this.getContacts(id)
            })
    }

    update(id, values) {
        this.updateObserver = this.MessagesService.putBackground(this.url + '/' + id, values)
            .subscribe(data => {
                this.getContacts(id)
            })
    }

    // id is contact id
    delete(id, contactid) {
        this.deletetObserver = this.MessagesService.deleteBackground(this.url + '/' + contactid)
            .subscribe(data => {
                this.getContacts(id)
            })
    }

    download(id) {
        this.downloadObserver = this.MessagesService.getBackground(this.url + '/download/' + id)
            .subscribe((data: any) => {
                console.log(`download`);
                console.log(data);
                // var blob = new Blob([JSON.stringify(data, null, 2)], { type: 'text/csv;' });
                var blob = new Blob([data.file], { type: 'text/csv' });

                var url = window.URL.createObjectURL(blob);
                // window.open(url);
                saveAs(blob, 'user_details.csv')
            }, err => {
                console.log(`error @ download`)
                console.log(err);
            })
    }


    ngOnDestroy() {
        this.getObserver.unsubscribe();
        this.insertObserver.unsubscribe();
        this.updateObserver.unsubscribe();
        this.deletetObserver.unsubscribe();
        this.downloadObserver.unsubscribe();

    }
}