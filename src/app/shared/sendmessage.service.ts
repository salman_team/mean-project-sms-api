import { MessagesService } from "./messages.service";

export class SendmessageService {
    private url = '/api/sendmessage/';

    constructor(private MessagesService: MessagesService) {

    }

    sendMessage(data) {
        this.MessagesService.postBackground(this.url, data)
    }

}