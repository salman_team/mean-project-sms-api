import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessagesService } from './messages.service';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';
import { Observer } from 'rxjs/Observer';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
@Injectable()


export class GroupsdataService implements OnInit, OnDestroy {
    groupsList;
    ObjectReturn: Observable<any>;
    public groupsObservable = new BehaviorSubject<any>([]);
    public groupCurrentList = this.groupsObservable.asObservable();

    private cartItemList = new BehaviorSubject<any>([]);
    public currentCartItemList = this.cartItemList.asObservable();
    public url = '/api/groups';
    private gropOberver;


    public testSubject = new Subject<any>();


    constructor(
        private MessagesService: MessagesService,
        private HttpClient: HttpClient
    ) { }
    
    ngOnInit() {

    }
    getGroups() {
        console.log(this.url);

        this.gropOberver = this.MessagesService.getBackground(this.url)
            .subscribe(data => {
                this.groupsList = data;
                this.groupsObservable.next(this.groupsList);
            })
    }

    addGroup(values) {
        this.MessagesService.postBackground(this.url, values)
            .subscribe(data => {
                console.log(`insert group `)
                console.log(data);
                this.getGroups()
                // this.groupsList.push(obj);
                this.groupsObservable.next(this.groupsList);
            }, err => {
                console.log(`error while insert`)
                console.log(err);
            })
    }

    removeGroups(index) {
        this.MessagesService.deleteBackground(this.url + '/' + index)
            .subscribe(data => {
                this.groupsList = data;
                this.groupsObservable.next(this.groupsList);
            })
    }



    editGroups(index) {

    }

    ngOnDestroy() {
        this.gropOberver.unsubscribe()
    }
} 