import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/operators'
@Injectable()
export class MessagesService implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {

  }
  getBackground(url) {
    return this.http.get(url).share();
  }

  postBackground(url, options) {
    console.log(`add group services ${JSON.stringify(options)}`)
    return this.http.post(url, options);
  }

  putBackground(url, options) {
    return this.http.put(url, options);
  }

  deleteBackground(url) {
    return this.http.delete(url);
  }
}
