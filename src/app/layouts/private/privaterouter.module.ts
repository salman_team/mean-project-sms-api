import { Routes, RouterModule } from '@angular/router';

import { PrivateComponent } from './private.component';
import { MessagesComponent } from '../../layoutchildrens/messages/messages.component'

const router: Routes = [
    {
        path: '', component: PrivateComponent,
        children: [
            { path: '', redirectTo: 'messages' },
            { path: 'messages', loadChildren: '../../layoutchildrens/messages/messages.module#MessagesModule' }
        ]
    }
]
/*


*/
export const privateRouterModule = RouterModule.forChild(router); 