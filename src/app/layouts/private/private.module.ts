import { NgModule } from '@angular/core';

import { privateRouterModule } from './privaterouter.module';
import { SharedModule } from '../../shared/shared.module';
import { MessagesModule } from '../../layoutchildrens/messages/messages.module'

import { PrivateComponent } from './private.component';
import { LoginComponent } from '../../components/login/login.component';
import { SignupComponent } from '../../components/signup/signup.component';


@NgModule({
    declarations: [
        PrivateComponent,

    ],
    imports: [
        privateRouterModule,
        SharedModule
    ]
})

export class PrivateModule {

}