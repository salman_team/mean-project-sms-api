import { Routes, RouterModule } from '@angular/router';

import { PublicComponent } from './public.component';
import { LoginComponent } from '../../components/login/login.component';
import { SignupComponent } from '../../components/signup/signup.component';

const router: Routes = [
    {
        path: '', component: PublicComponent,
        children: [
            { path: '', redirectTo:'login' },
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignupComponent }
        ]
    }
]
/*


*/
export const publicRouterModule = RouterModule.forChild(router); 