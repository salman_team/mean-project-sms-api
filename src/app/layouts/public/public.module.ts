import { NgModule } from '@angular/core';

import { publicRouterModule } from './publicrouter.module';
import { SharedModule } from '../../shared/shared.module';

import { PublicComponent } from './public.component';
import { LoginComponent } from '../../components/login/login.component';
import { SignupComponent } from '../../components/signup/signup.component';


@NgModule({
    declarations: [
        PublicComponent,
        LoginComponent,
        SignupComponent
    ],
    imports: [
        publicRouterModule,
        SharedModule
    ]
})

export class PublicModule {

}