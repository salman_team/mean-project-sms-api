import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routers: Routes = [
    {
        path: '', loadChildren: './layouts/public/public.module#PublicModule',
    },
    //secondary private layout dashboard
    {
        path: '', loadChildren: './layouts/private/private.module#PrivateModule',

    },
    { path: '**', redirectTo: '' }

]

export const AppRouterModule = RouterModule.forRoot(routers)
