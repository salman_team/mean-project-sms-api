import { Component, OnInit } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms'
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx'

import { GroupsdataService } from '../../shared/groupsdata.service';
import { MessagesService } from '../../shared/messages.service';
import { ContactsdataService } from '../../shared/contactsdata.service';


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})

export class GroupsComponent implements OnInit {
  public groupList = [];
  localObsever;
  name = 'welcome'
  constructor(
    private gropService: GroupsdataService,
    private fb: FormBuilder,
    private MessagesService: MessagesService,
    private ContactsdataService:ContactsdataService
  ) {

    this.gropService.testSubject
      .subscribe(data => {
        alert(data);
      })
  }


  ngOnInit() {

    this.gropService.getGroups()
    this.localObsever = this.gropService.groupCurrentList
      .subscribe(groupList => {
        if (!groupList.length)
          return;

        console.log(groupList)
        this.groupList = groupList;
      })
  }

  editGroup(index) {

  }

  deleteGroup(i) {
    let id = this.groupList[i].id
    this.gropService.removeGroups(id)
    console.log(`************** delete block **************`);
  }

  download(i){
    let id = this.groupList[i].id
    this.ContactsdataService.download(id)
  }


  addGroupForm = this.fb.group({
    name: ['salman']
  })

  formSubmit(values) {
    console.log(`submited`)
    console.log(values);
    this.gropService.addGroup(values)
  }

  ngOnDestroy() {
    this.localObsever.unsubscribe()
  }
} 
