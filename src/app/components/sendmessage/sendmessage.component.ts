import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessagesService } from '../../shared/messages.service';


@Component({
  selector: 'app-sendmessage',
  templateUrl: './sendmessage.component.html',
  styleUrls: ['./sendmessage.component.css']
})
export class SendmessageComponent implements OnInit {
  sendsms: FormGroup;
  myform: FormGroup;
  url = 'api/sendmessages'
  constructor(private fb: FormBuilder, private MessagesService: MessagesService) {

    this.sendsms = this.fb.group({
      sendsms1: this.fb.group({
        sender: ['salman'],
        route: [4],
        country: [91],
        message: ['welcome sample message'],
        mobiles: [8056947504],
      })

    })

    // http://api.msg91.com/api/sendhttp.php?sender=MSGIND&route=4&country=0&message=Hello! This is a test message

    // this.myform = this.fb.group({
    //   group1: this.fb.group({
    //     sender: ['salman'],
    //     route: [8056947504],
    //   }),
    //   group2: this.fb.group({
    //     country: ['rr complex'],
    //     message: ['karur'],
    //     mobiles: [639005],
    //     city: ['karur']
    //   })
    // })
  }

  ngOnInit() {

  }

  smsFormSubmit(values) {
    console.log(values)
    this.MessagesService.postBackground(this.url, values.sendsms1)
      .subscribe(data => {
        console.log(`data recived from send message block`)
        console.log(data);
      }, err => {
        console.log(`______________ ERROR @ send message __________`)
        console.log(err);
      })


  }



  myformSubmit(values) {
    console.log(values)
  }

}


