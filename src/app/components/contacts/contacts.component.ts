import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessagesService } from '../../shared/messages.service';
import { ContactsdataService } from '../../shared/contactsdata.service';
import { ActivatedRoute } from '@angular/router';
import { GroupsdataService } from '../../shared/groupsdata.service';
import { FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  contactList;
  groupList;
  id;
  private contactObservable;
  private groupObervabel;
  private routerObs;
  private groupid;
  constructor(
    private MessagesService: MessagesService,
    private ContactsdataService: ContactsdataService,
    private ActivatedRoute: ActivatedRoute,
    private GroupsdataService: GroupsdataService,
    private fb: FormBuilder
  ) {
    this.routerObs = this.ActivatedRoute.params
      .subscribe(params => {
        this.id = params.id;

      });
  }

  ngOnInit() {
    this.groupObervabel = this.GroupsdataService.groupsObservable
      .subscribe(list => {
        if (!list || !list.length) {
          this.GroupsdataService.getGroups();
          return;
        }
        this.groupid = list[this.id].id;
        this.addForm.patchValue({ group: this.groupid })
        console.log(this.groupid)
        this.ContactsdataService.getContacts(this.groupid)
      })

    this.contactObservable = this.ContactsdataService.contactObservable
      .subscribe(data => {
        console.log(`contac list recived `)
        console.log(data)
        this.contactList = data;
      }, err => {
        console.log(err);
      })
  }

  delete(i) {
    let contactid = this.contactList[i].contactid;
    this.ContactsdataService.delete(this.groupid, contactid)
  }

  edit(i) {
    console.log(`edit ${i}`);
    let currentUser = this.contactList[i]
    this.editForm.patchValue(currentUser)
    this.editForm.patchValue({ mob_no: currentUser.number, contact_id:currentUser.contactid, group:this.groupid })
  }

  addForm = this.fb.group({
    name: [''],
    group: [''],
    mob_no: ['']
  })

  editForm = this.fb.group({
    name: [''],
    group: [''],
    mob_no: [''],
    contact_id: ['']
  })


  formSubmit(values) {
    console.log(values)
    this.ContactsdataService.insert(this.groupid, values)
  }

  formEditSubmit(values) {
    this.ContactsdataService.update(this.groupid, values)
  }

  ngOnDestroy() {
    this.contactObservable.unsubscribe();
    this.groupObervabel.unsubscribe();
    this.routerObs.unsubscribe()
  }
}
