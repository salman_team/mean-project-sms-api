import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaginsComponent } from './campagins.component';

describe('CampaginsComponent', () => {
  let component: CampaginsComponent;
  let fixture: ComponentFixture<CampaginsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaginsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaginsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
