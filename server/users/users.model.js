var mongoose = require('mongoose');

var users = mongoose.Schema({
    username: String,
    password: String,
    email: String,
    lastlogin: Date,
}, { collection: 'users' })


var usersModel = mongoose.model('users', users);
/*
var newusers = new usersModel({
    username: 'salman',
    password: 'oneindia_123',
    email: 'salman@iaaxin.com'
})

newusers.save(newusers, function (err, rec) {

    console.log(`save successfully `)
    console.log(rec)
})
*/
module.exports = usersModel