var https = require('https');
var config = require('../configs/smsconfig')
var postoption = {
    method: 'GET',
    hostname: 'control.msg91.com',
    path: '/api/list_group.php',
    port: null,
    headers: {}
}

function handleError(req, res) {
    return function (err) {
        console.log(`----------------- error occured -----------------`)
        console.log(JSON.stringify(err));
        res.status(500).send(err);
    }
}

function checkApiError() {
    return function (data) {
        if (data.msgType !== 'success') {
            throw new Error(data);
            return;
        }
        return data;
    }
}

function callApi(options) {
    return new Promise((resolve, reject) => {
        var reqOptions = Object.assign(postoption, options)
        var request = https.request(reqOptions, function (respond) {
            var chunks = [];
            respond.on('data', function (str) {
                chunks.push(str);
                console.log(`data chunks recevied `)
            })
            respond.on("end", function () {
                var body = Buffer.concat(chunks).toString();
                body = JSON.parse(body)
                console.log(body)
                resolve(body)
            });
            respond.on('error', function (err) {
                console.log(`error occured while call`)
                console.log(err)
            })
        })
        request.end();
    })
}

exports.get = function (req, res) {
    var options = {
        path: `/api/list_group.php?authkey=${config.authkey}`,
        method: 'GET'
    }

    callApi(options)
        .then(data => {
            console.log(data)
            res.status(200).json(data)
        })
        .catch(err => {
            console.log(`error `)
            console.log(err);
        })

}


exports.insert = function (req, res) {
    console.log(`insert block ______________`)
    console.log(req.body);
    var name = req.body.name;
    var options = {
        path: `/api/add_group.php?authkey=${config.authkey}&group_name=${name}`,
        method: 'GET'
    }
    console.log(options)
    callApi(options)
        .then(data => {
            res.status(200).json(data)
        }).catch(err => {
            res.status(500).err(err)
        })
}

exports.delete = function (req, res) {
    var id = req.params.id;
    console.log(`params ${id}`);
    var options = {
        path: `/api/delete_group.php?authkey=${config.authkey}&group_id=${id}`,
        method: 'GET'
    }
    var getOptions = {
        path: `/api/list_group.php?authkey=${config.authkey}`,
        method: 'GET'
    }
    callApi(options)
        .then(checkApiError())
        .then(data => {
            return callApi(getOptions)
            // .then(data => { return data })
        })
        .then(function (data) {
            console.log(`get data response ---------------`)
            console.log(data);
            res.status(200).json(data)
        })
        .catch(handleError(req, res))
} 