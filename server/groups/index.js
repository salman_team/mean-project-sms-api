var express = require('express');
var router = express.Router();
var groupsCtrl = require('./groups.controller')



router.get('/', groupsCtrl.get)
router.post('/', groupsCtrl.insert)
router.delete('/:id', groupsCtrl.delete)


 
module.exports = router;