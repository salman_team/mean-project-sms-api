var users = require('./users/index');
var groups = require('./groups/index');
var contacts = require('./contacts/index');
var sendmessages = require('./sendsms/index')

var appRoot = function (app) {
    app.use('/api/users', users)
    app.use('/api/groups', groups)
    app.use('/api/contacts', contacts)
    app.use('/api/sendmessages', sendmessages)
}


module.exports = appRoot;