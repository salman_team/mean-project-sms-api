var express = require('express')
var router = express.Router();
var contactsCtrl = require('./contacts.controller');

router.post('/', contactsCtrl.insert)
router.put('/:id', function(req, res, next){console.log(`put request block`); next()} , contactsCtrl.update)
router.get('/:id', contactsCtrl.get)
router.delete('/:id', contactsCtrl.delete)
router.get('/download/:id', contactsCtrl.download)

module.exports = router;