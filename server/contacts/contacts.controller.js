var https = require('https');
var config = require('../configs/smsconfig')
var qs = require('querystring')
const Json2csvParser = require('json2csv').Parser;

var postoption = {
    method: 'GET',
    hostname: 'control.msg91.com',
    path: '/api/list_group.php',
    port: null,
    headers: {}
}

function handleError(req, res) {
    return function (err) {
        console.log(`----------------- error occured -----------------`)
        console.log(JSON.stringify(err));
        res.status(500).send(err);
    }
}

function checkApiError() {
    return function (data) {

        console.log(`response body body`)
        console.log(data)
        type = (Array.isArray(data) ? data.pop().msgType : data.msg_type)
        console.log(`type of ${type}`)

        if (!type || type !== 'success') {
            throw new Error(data);
            return;
        }
        return data;
    }
}

function callApi(options) {
    return new Promise((resolve, reject) => {
        var reqOptions = Object.assign(postoption, options)
        var request = https.request(reqOptions, function (respond) {
            var chunks = [];
            respond.on('data', function (str) {
                chunks.push(str);
                console.log(`data chunks recevied `)
            })
            respond.on("end", function () {
                var body = Buffer.concat(chunks).toString();
                body = JSON.parse(body)
                console.log(body)
                resolve(body)
            });
            respond.on('error', function (err) {
                console.log(`error occured while call`)
                console.log(err)
            })
        })
        request.end();
    })
}


exports.get = function (req, res) {
    var options = {
        path: `/api/list_contact.php?authkey=${config.authkey}`,
        method: 'GET'
    }

    config.group = req.params.id; //req.params.id;

    options = {
        path: `/api/list_contact.php?${qs.stringify(config)}`,
        method: 'GET'
    }
    console.log(`************ query string ************`)
    console.log(options)
    console.log(qs.stringify(config))

    callApi(options)
        .then(checkApiError())
        .then(data => {
            console.log(data)
            res.status(200).json(data)
        })
        .catch(handleError(req, res))

}

exports.insert = function (req, res) {
    console.log(`insert block ______________`)
    console.log(req.body);

    var params = Object.assign(config, req.body)

    var options = {
        path: `/api/add_contact.php?${qs.stringify(params)}`,
        method: 'GET'
    }
    console.log(options)
    callApi(options)
        .then(data => {
            res.status(200).json(data)
        }).catch(err => {
            res.status(500).err(err)
        })
}

exports.delete = function (req, res) {
    var id = req.params.id;
    console.log(`params ${id}`);
    var options = {
        path: `/api/delete_contact.php?authkey=${config.authkey}&contact_id=${id}`,
        method: 'GET'
    }
    var getOptions = {
        path: `/api/list_contact.php?authkey=${config.authkey}`,
        method: 'GET'
    }
    callApi(options)
        .then(checkApiError())
        .then(function (data) {
            console.log(`get data response ---------------`)
            console.log(data);
            res.status(200).json(data)
        })
        .catch(handleError(req, res))



}

exports.update = function (req, res) {
    var params = Object.assign(config, req.body)

    var options = {
        path: `/api/edit_contact.php?${qs.stringify(params)}`,
        method: 'GET'
    }
    console.log(options)

    callApi(options)
        .then(data => {
            res.status(200).json(data)
        }).catch(err => {
            res.status(500).err(err)
        })
}


exports.download = function (req, res) {
    var options = {
        path: `/api/list_contact.php?authkey=${config.authkey}`,
        method: 'GET'
    }


    config.group = req.params.id; //req.params.id;

    options = {
        path: `/api/list_contact.php?${qs.stringify(config)}`,
        method: 'GET'
    }
    console.log(`************ query string ************`)
    console.log(options)
    console.log(qs.stringify(config))

    callApi(options)
        .then(checkApiError())
        .then(data => {
            console.log(data)

            const fields = Object.keys(data[0]);
            const opts = { fields };
            try {
                const csv = new Json2csvParser(opts).parse(data);
                console.log(csv);
                // var filename = 'user.json';
                // var mimetype = 'application/json';
                // res.setHeader('Content-Type', mimetype);
                // res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                res.send({file:csv});
            } catch (err) {
                console.error(err);
            }

            // var json = require('./demo.json');
            var user = { "name": "azraq", "country": "egypt" };
            var json = JSON.stringify(data);
            // var filename = 'user.json';
            // var mimetype = 'application/json';
            // res.setHeader('Content-Type', mimetype);
            // res.setHeader('Content-disposition', 'attachment; filename=' + filename);


        })
        .catch(handleError(req, res))

}