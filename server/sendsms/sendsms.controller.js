var https = require('https');
var config = require('../configs/smsconfig')
var qs = require('querystring')


var postoption = {
    method: 'GET',
    hostname: 'control.msg91.com',
    path: '/api/list_group.php',
    port: null,
    headers: {}
}

function handleError(req, res) {
    return function (err) {
        console.log(`----------------- error occured -----------------`)
        console.log(JSON.stringify(err));
        res.status(500).send(err);
    }
}

function checkApiError() {
    return function (data) {

        console.log(`response body body`)
        console.log(data)
        type = (Array.isArray(data) ? data.pop().msgType : data.msg_type)
        console.log(`type of ${type}`)

        if (!type || type !== 'success') {
            throw new Error(data);
            return;
        }
        return data;
    }
}

function callApi(options) {
    return new Promise((resolve, reject) => {
        var reqOptions = Object.assign(postoption, options)
        var request = https.request(reqOptions, function (respond) {
            var chunks = [];
            respond.on('data', function (str) {
                chunks.push(str);
                console.log(`data chunks recevied `)
            })
            respond.on("end", function () {
                var body = Buffer.concat(chunks).toString();
                try {
                    body = JSON.parse(body)
                } catch (e) {
                    console.log(body)
                    reject(body);
                    return;
                }
                console.log(body)
                resolve(body)
            });
            respond.on('error', function (err) {
                console.log(`error occured while call`)
                console.log(err)
            })
        })

        if (reqOptions.method.toUppercase() == 'POST')
            request.write(JSON.stringify(reqOptions.body))


        request.end();
    })
}

exports.send = function (req, res) {
    var params = Object.assign(config, req.body)

    var options = {
        path: `/api/sendhttp.php?${qs.stringify(params)}`,
        method: 'GET'
    }
    console.log(options)
    callApi(options)
        .then(checkApiError())
        .then(data => {
        })
        .catch(handleError(req, res))
}


exports.sendv2 = function (req, res) {
    var params = Object.assign(config, req.body)
    var msgBody = {
        sender: 'SOCKET',
        route: '4',
        country: '91',
        sms:
            [{ message: 'Message1', to: ['9597590975', '9597590975'] },
            { message: 'Message2', to: ['9597590975', '9597590975'] }]

    }
    var options = {
        path: `/api/v2/sendsms`,
        method: 'POST',
        body: msgBody
    }

    console.log(options)
    callApi(options)
        .then(checkApiError())
        .then(data => {
        })
        .catch(handleError(req, res))
}
